#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Niels Sandholt Busch'
SITENAME = u'resmio'
SITEURL = ''

PAGE_DIR=''
ARTICLE_DIR='blog'

PAGE_URL='{slug}.html'
PAGE_SAVE_AS='{slug}.html'
# PAGE_LANG_URL=''
# PAGE_LANG_SAVE_AS=''

PLUGIN_PATH = "plugins"
PLUGINS = ["i18n_subsites"]

JINJA_EXTENSIONS = ['jinja2.ext.i18n']

TIMEZONE = 'Europe/Berlin'

DEFAULT_LANG = u'en'
LOCALE = u'en_US'

I18N_SUBSITES = {
    'de': { 
        'SITENAME': 'resmio',
        'LOCALE': 'de_DE',
    },
}

I18N_GETTEXT_LOCALEDIR = 'themes/resmio/translations'
I18N_GETTEXT_DOMAIN = 'messages'

languages_lookup = {
    'en': 'English',
    'de': 'Deutsch',
}

def lookup_lang_name(lang_code):
    return languages_lookup[lang_code]

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

DEFAULT_PAGINATION = False

THEME = "themes/resmio"

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
